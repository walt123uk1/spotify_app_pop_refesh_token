# spotify_app

## Project setup
```
npm install
Moved requests to js file as that is on spotify demo. 
Unable to access apis from vue components to get token.
This could refresh token and keep sensitive info away from front end.

Very close but can't find examples of how to use promises and return json using a javascript function.

```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
